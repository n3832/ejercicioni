# API USUARIOS

Ejemplo de api para autenticacion y creacion de usuarios

# FRAMEWORKS UTILIZADOS

1. Spring boot
2. Hibernate
3. Lombok
4. H2
5. Swagger
6. Json web Token

# Ejecución de endpoints

Se debe de compilar el proyecto con el siguiente comando

```shell
./gradlew bootRun
```

Luego se pueden ejecutar los endpoints mediante la url http://localhost:8080/

### GET /api/user/v1/find
Devuelve un usuario por email

**Parameters**

|            Nombre | Requerido |  Tipo  | Descripcion                                                                                                                                                                                       |
|------------------:|:---------:|:------:|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|           `email` | required  | string | El email del usuario a buscar                                                                                                                                                                     |

### POST /api/user/v1/sign_up
Crea un nuevo usuario

**Parameters**

| Nombre | Requerido |  Tipo   | Descripcion                          |
|-------:|:---------:|:-------:|--------------------------------------|
| `user` | required  | UserDto | El objeto json del usuario a guardar |

**Objeto json**

```
{
"name": "juan",
"email": "juan@rodriguez.org",
"password": "Hunter24",
"phones": [
{
"number": "1234567",
"citycode": "1",
"contrycode": "57"
}
]
}

```
### PUT /api/user/v1/disable/{id}
Desactiva un usuario

**Parameters**

| Nombre | Requerido | Tipo | Descripcion                    |
|-------:|:---------:|:----:|--------------------------------|
|   `id` | required  | UUID | El id del usuario a desactivar |

### PUT /api/user/v1/update/{id}
Actualiza un usuario

**Parameters**

| Nombre | Requerido |  Tipo   | Descripcion                         |
|-------:|:---------:|:-------:|-------------------------------------|
|   `id` | required  |  UUID   | El id del usuario a desactivar      |
| `user` | required  | UserDto | El objeto json con los nuevos datos |

**Objeto json**
```
{
"name": "pedro Rodriguez",
"email": "juan@rodriguez.org",
"password": "Hunter24",
"phones": [
{
"number": "1234567",
"citycode": "1",
"contrycode": "57"
}
]
}

```

### POST /api/user/v1/sign_in
Inicia sesion

**Parameters**

|     Nombre | Requerido |  Tipo  | Descripcion              |
|-----------:|:---------:|:------:|--------------------------|
|    `email` | required  | String | El email del usuario     |
| `password` | required  | String | La contaseña del usuario |

#DIAGRAMA

![alt text](diagrama.png)