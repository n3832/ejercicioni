DROP TABLE IF EXISTS user;
CREATE TABLE user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(100) NOT NULL,
    created TIMESTAMP NOT NULL,
    modified TIMESTAMP,
    last_login TIMESTAMP NOT NULL,
    token VARCHAR(300),
    isactive BOOLEAN NOT NULL

);

DROP TABLE IF EXISTS phone;
CREATE TABLE phone(
   id INT AUTO_INCREMENT PRIMARY KEY,
   user_id INT NOT NULL,
   number VARCHAR(25) NOT NULL,
   city_code VARCHAR(3) NOT NULL,
   country_code varchar(3) NOT NULL,
   FOREIGN KEY (user_id) REFERENCES user(id)
)