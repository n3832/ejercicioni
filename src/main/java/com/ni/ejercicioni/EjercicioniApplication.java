package com.ni.ejercicioni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EjercicioniApplication {

    public static void main(String[] args) {
        SpringApplication.run(EjercicioniApplication.class, args);
    }

}
