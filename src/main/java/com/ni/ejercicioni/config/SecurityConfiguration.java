package com.ni.ejercicioni.config;

import com.ni.ejercicioni.util.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private TokenGenerator tokenGenerator;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.csrf().disable().httpBasic().and()
                .authorizeRequests()
                .antMatchers("/api/user/v1/sign_up/", "/api/user/v1/sign_in/", "/h2-console/**", "/swagger-resources/**", "/swagger-ui.html", "/v2/api-docs", "/swagger-ui/**").permitAll()
                .anyRequest().authenticated()
                .and().addFilterAfter(new TokenFilter(tokenGenerator), BasicAuthenticationFilter.class);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/api/user/v1/sign_up/**", "/api/user/v1/sign_in/**", "/h2-console/**", "/swagger-resources/**", "/swagger-ui/**", "/swagger-ui.html", "/v2/api-docs");
    }
}
