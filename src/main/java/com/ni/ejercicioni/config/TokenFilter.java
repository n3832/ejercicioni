package com.ni.ejercicioni.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ni.ejercicioni.dto.ErrorMessage;
import com.ni.ejercicioni.util.TokenGenerator;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.header.HeaderWriterFilter;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class TokenFilter extends GenericFilterBean {

    private final TokenGenerator tokenGenerator;

    public TokenFilter(TokenGenerator tokenGenerator) {
        this.tokenGenerator = tokenGenerator;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String token = ((HttpServletRequest) request).getHeader("access-token");
        HttpServletResponse resp = (HttpServletResponse) response;

        if (token == null) {
            ErrorMessage errorMessage = new ErrorMessage("E05", "Debe de proveer el token de seguridad", null);
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            resp.getWriter().write(convertObjectToJson(errorMessage));
        } else if (!tokenGenerator.validateToke(token)) {
            ErrorMessage errorMessage = new ErrorMessage("E06", "El token de seguridad es invalido", null);
            resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            resp.getWriter().write(convertObjectToJson(errorMessage));
        }
        chain.doFilter(request, response);
    }

    public String convertObjectToJson(Object object) throws JsonProcessingException {
        if (object == null) {
            return null;
        }
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }
}
