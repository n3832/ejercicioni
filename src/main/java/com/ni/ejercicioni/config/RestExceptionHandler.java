package com.ni.ejercicioni.config;

import com.ni.ejercicioni.dto.ErrorMessage;
import com.ni.ejercicioni.exception.BusinessException;
import com.ni.ejercicioni.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@ControllerAdvice
public class RestExceptionHandler {

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorMessage handleNotFoundException(NotFoundException ex) {
        return new ErrorMessage("E01", ex.getMessage(), null);
    }

    @ResponseBody
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleConstraintException(ConstraintViolationException ex) {
        Optional<ConstraintViolation<?>> optional = ex.getConstraintViolations().stream().findFirst();
        if (optional.isPresent()) {
            return new ErrorMessage("E02", optional.get().getMessage(), null);
        } else {
            return new ErrorMessage("E02", ex.getMessage(), null);
        }
    }

    @ResponseBody
    @ExceptionHandler(BusinessException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleBusinessException(BusinessException ex) {
        return new ErrorMessage("E03", ex.getMessage(), null);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }


    @ResponseBody
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorMessage handleSqlException(SQLIntegrityConstraintViolationException ex) {
        if (ex.getMessage().contains("EMAIL_UNQ") && ex.getSQLState().equals("23505")) {
            return new ErrorMessage("E4", "El email ya se encuentra registrado",null);
        } else {
            return new ErrorMessage("E0", ex.getMessage(), null);
        }

    }

}
