package com.ni.ejercicioni.util;

import com.ni.ejercicioni.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Base64;
import java.util.Date;

@Component
public class TokenGenerator {

    @Value("${ejercicioni.secreto}")
    private String secreto;

    @Value("${ejercicioni.expiracion}")
    private Integer expiracion;

    public String generateToken(User user) {
        return Jwts.builder()
                .setIssuer(user.getName())
                .setSubject(user.getEmail())
                .setIssuedAt(new Date())
                .setExpiration(Date.from(LocalDateTime.now().plusMinutes(expiracion).atZone(ZoneId.systemDefault()).toInstant()))
                .signWith(
                        SignatureAlgorithm.HS256,
                        TextCodec.BASE64.decode(secreto)
                ).compact();
    }

    public Boolean validateToke(String token) {
        try {
            Key hmacKey = new SecretKeySpec(TextCodec.BASE64.decode(secreto),
                    SignatureAlgorithm.HS256.getJcaName());

            Claims jwt = Jwts.parser()
                    .setSigningKey(hmacKey)
                    .parseClaimsJws(token).getBody();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
