package com.ni.ejercicioni.service;

import com.ni.ejercicioni.dao.UserRepository;
import com.ni.ejercicioni.dto.UserDto;
import com.ni.ejercicioni.entity.User;
import com.ni.ejercicioni.exception.BusinessException;
import com.ni.ejercicioni.exception.NotFoundException;
import com.ni.ejercicioni.util.TokenGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
@Transactional
public class UserService {

    private final UserRepository userDao;
    private final TokenGenerator tokenGenerator;


    @Autowired
    public UserService(UserRepository userDao, TokenGenerator tokenGenerator) {
        this.userDao = userDao;
        this.tokenGenerator = tokenGenerator;
    }

    public Optional<UserDto> findByEmail(String email) throws NotFoundException {
        User user = userDao.findUserByEmail(email).orElse(null);
        if (user != null) {
            return Optional.of(crearDto(user));
        } else {
            return Optional.empty();
        }
    }

    public User save(User user) throws Exception {
        String regexPassword = "^(?=.*[0-9]{2})"
                + "(?=.*[a-z])(?=.*[A-Z])"
                + "(?=\\S+$).{8,20}$";

        Pattern pattern = Pattern.compile(regexPassword);
        Matcher matcher = pattern.matcher(user.getPassword());
        if (!matcher.matches()) {
            throw new BusinessException("La contraseña es invalida");
        }
        user.setToken(tokenGenerator.generateToken(user));
        user.setLastLogin(new Timestamp(new Date().getTime()));
        user.setIsActive(true);
        user.getPhones().forEach(w -> w.setUser(user));
        return userDao.save(user);
    }

    public UserDto disable(UUID id) throws Exception {
        Optional<User> user = userDao.findById(id);
        if (user.isEmpty()) {
            throw new BusinessException("Usuario no encontrado");
        } else {
            User usr = user.get();
            usr.setIsActive(false);
            userDao.save(usr);
            return crearDto(usr);
        }
    }

    public UserDto modificar(UserDto user, UUID id) throws Exception {
        Optional<User> optionalUser = userDao.findById(id);
        if (optionalUser.isEmpty()) {
            throw new BusinessException("Usuario no encontrado");
        } else {
            User usr = optionalUser.get();
            usr.setName(user.getName());
            usr.setModified(new Timestamp(new Date().getTime()));
            userDao.save(usr);
            return crearDto(usr);
        }
    }

    public UserDto signIn(String email, String password) throws BusinessException {
        User optionalUser = userDao.findUserByEmail(email).orElseThrow(() -> new BusinessException("Usuario no encontrado"));
        if (!password.equals(optionalUser.getPassword())) {
            throw new BusinessException("La contrasenia es incorrecta");
        }
        return crearDto(optionalUser);
    }

    private UserDto crearDto(User usr) {
        return new UserDto(usr.getId(), usr.getName(), usr.getEmail(), usr.getToken(), "", usr.getPhones(), usr.getIsActive());
    }
}
