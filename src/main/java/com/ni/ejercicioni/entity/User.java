package com.ni.ejercicioni.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "user", uniqueConstraints = {@UniqueConstraint(columnNames = {"email"}, name = "email_unq")})
@Getter
@Setter
public class User implements Serializable {

    @Id
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @GeneratedValue(generator = "uuid")
    private UUID id;

    @Basic
    @Column(name = "name")
    @NotNull(message = "{name.notNull}")
    private String name;

    @Basic
    @Column(name = "email")
    @NotNull(message = "{email.notNull}")
    @Email(message = "{email.notValid}")
    private String email;

    @Basic
    @Column(name = "password")
    @NotNull(message = "{password.notNull}")
    private String password;

    @Basic
    @Column(name = "created")
    @CreationTimestamp
    private Timestamp created;

    @Basic
    @Column(name = "modified")
    @UpdateTimestamp
    private Timestamp modified;

    @Basic
    @Column(name = "last_login")
    @NotNull(message = "{lastLogin.notNull}")
    private Timestamp lastLogin;

    @Basic
    @Column(name = "token")
    @NotNull(message = "{token.notNull}")
    private String token;

    @Basic
    @Column(name = "isactive")
    @NotNull(message = "{status.notNull}")
    private Boolean isActive;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    List<Phone> phones;
}
