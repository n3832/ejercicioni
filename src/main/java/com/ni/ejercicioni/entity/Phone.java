package com.ni.ejercicioni.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "phone")
@Getter
@Setter
public class Phone {

    @ApiModelProperty(value = "El id del telfono",
            name = "id",
            dataType = "Integer",
            required = false,
            position = 1
    )
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    @JsonIgnore
    private User user;

    @ApiModelProperty(value = "El numero de telfono",
            name = "number",
            dataType = "String",
            required = true,
            position = 2
    )
    @Basic
    @Column(name = "number")
    @NotNull(message = "Numero requerido")
    private String number;

    @ApiModelProperty(value = "El codigo de la ciudad",
            name = "city_code",
            dataType = "String",
            required = true,
            position = 3
    )
    @Basic
    @Column(name = "city_code")
    @NotNull(message = "Codigo de la ciudad requerido")
    @JsonProperty("citycode")
    private String cityCode;


    @ApiModelProperty(value = "El codigo del pais",
            name = "countryCode",
            dataType = "String",
            required = true,
            position = 4
    )
    @Basic
    @Column(name = "country_code")
    @NotNull(message = "Codigo del pais requerido")
    @JsonProperty("contrycode")
    private String countryCode;
}
