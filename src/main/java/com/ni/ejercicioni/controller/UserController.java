package com.ni.ejercicioni.controller;

import com.ni.ejercicioni.dto.InfoMessage;
import com.ni.ejercicioni.dto.UserDto;
import com.ni.ejercicioni.entity.User;
import com.ni.ejercicioni.exception.NotFoundException;
import com.ni.ejercicioni.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Valid
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "v1/find")
    @ApiOperation(value = "Devuelve un usuario segun su email",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "usuario encontrado", response = UserDto.class),
            @ApiResponse(code = 500, message = "Usuario no encontrado")
    })
    public UserDto findByEmail(@RequestParam(required = false) String email) throws Exception {
        return userService.findByEmail(email).orElseThrow(() -> new NotFoundException("Usuario no encontrado"));
    }

    @ApiOperation(value = "Crea un nuevo usuario",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Guardado correctamente", response = InfoMessage.class),
            @ApiResponse(code = 500, message = "Contraseña es invalida"),
            @ApiResponse(code = 500, message = "El email ya se encuentra registrado")
    })
    @PostMapping(value = "v1/sign_up")
    public ResponseEntity<InfoMessage> saveUser(@RequestBody UserDto user, UriComponentsBuilder builder) throws Exception {
        User userEntity = new User();
        userEntity.setName(user.getName());
        userEntity.setEmail(user.getEmail());
        userEntity.setPassword(user.getPassword());
        userEntity.setPhones(new ArrayList<>(user.getPhones()));
        User savedUser = userService.save(userEntity);
        builder.path("v1/find").queryParam("email", savedUser.getEmail());
        return ResponseEntity.created(builder.build().toUri()).header("access-token", savedUser.getToken()).body(new InfoMessage("Guardado correctamente"));
    }

    @ApiOperation(value = "Desactiva un usuario",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "desactivado correctamente", response = UserDto.class),
            @ApiResponse(code = 500, message = "Usuario no encontrado")
    })
    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping(value = "v1/disable/{id}")
    public ResponseEntity<UserDto> disableUser(@PathVariable UUID id) throws Exception {
        return ResponseEntity.ok(userService.disable(id));
    }

    @ApiOperation(value = "Endpoint que actualiza un usuario",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "atualizado correctamente", response = UserDto.class),
            @ApiResponse(code = 500, message = "Usuario no encontrado")
    })
    @PreAuthorize("hasRole('USER')")
    @PutMapping(value = "v1/update/{id}")
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto user, @PathVariable UUID id) throws Exception {
        return ResponseEntity.ok(userService.modificar(user, id));
    }

    @ApiOperation(value = "Inicia sesion segun el email y la contraseña",
            produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Inicia sesion correctamente", response = UserDto.class),
            @ApiResponse(code = 500, message = "Usuario no encontrado"),
            @ApiResponse(code = 500, message = "Contraseña incorrecta")
    })
    @PostMapping(value = "v1/sign_in")
    public ResponseEntity<UserDto> signIn(@RequestParam String email, @RequestParam String password) throws Exception {
        UserDto user = userService.signIn(email, password);
        return ResponseEntity.ok(user);
    }
}
