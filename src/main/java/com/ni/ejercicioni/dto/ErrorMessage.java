package com.ni.ejercicioni.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class ErrorMessage {

    private String statusCode;
    private String errorMessage;
    private String detail;
}
