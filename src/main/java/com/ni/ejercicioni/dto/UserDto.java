package com.ni.ejercicioni.dto;

import com.ni.ejercicioni.entity.Phone;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Getter
@Setter
public class UserDto implements Serializable {

    @ApiModelProperty(value = "El id del usuario",
            name = "id",
            dataType = "UUID",
            required = false,
            position = 1
    )
    private UUID id;
    @ApiModelProperty(value = "El nombre del usuario",
            name = "name",
            dataType = "String",
            required = true,
            position = 2
    )
    private String name;
    @ApiModelProperty(value = "El email del usuario",
            name = "email",
            dataType = "String",
            required = true,
            position = 3
    )
    private String email;
    @ApiModelProperty(value = "El token de seguridad asignado al usuario",
            name = "token",
            dataType = "String",
            required = false,
            position = 4
    )
    private String token;
    @ApiModelProperty(value = "La contraseña del usuario",
            name = "password",
            dataType = "String",
            required = true,
            position = 5
    )
    private String password;
    @ApiModelProperty(value = "La lista de telefonos del usuario",
            name = "phones",
            dataType = "Collection",
            required = true,
            position = 6
    )
    private Collection<Phone> phones;
    @ApiModelProperty(value = "El estado del usuario",
            name = "isActive",
            dataType = "boolean",
            required = false,
            position = 7
    )
    private boolean isActive;

    public UserDto() {
    }
}
