package com.ni.ejercicioni;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ni.ejercicioni.dao.UserRepository;
import com.ni.ejercicioni.entity.Phone;
import com.ni.ejercicioni.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EjercicioniApplicationTests {
    private static final ObjectMapper om = new ObjectMapper();

    @Autowired
    UserRepository userDao;

    @Autowired
    private MockMvc mockMvc;

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testFindEmail() throws Exception {
        User expectedUser = new User();
        expectedUser.setName("test");
        expectedUser.setEmail("test@gmail.com");
        expectedUser.setPassword("Abcdef12");
        Phone phone = new Phone();
        phone.setUser(expectedUser);
        phone.setCityCode("01");
        phone.setCountryCode("02");
        phone.setNumber("123456789");
        expectedUser.setPhones(new ArrayList<Phone>() {{
            add(phone);
        }});

        expectedUser = om.readValue(mockMvc.perform(post("/api/user/v1/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(expectedUser)))

                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), User.class);

        User actualUser = om.readValue(mockMvc.perform(get("/api/user/v1/find?email=test@gmail.com"))
                .andDo(print())
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString(), User.class);
        assertThat(expectedUser.getEmail()).isEqualTo(actualUser.getEmail());
    }

    @Test
    @WithMockUser(username = "test", password = "test")
    public void testSave() throws Exception {
        User expectedUser = new User();
        expectedUser.setName("jerry");
        expectedUser.setEmail("jerry@gmail.com");
        expectedUser.setPassword("$Abc.1234567");
        Phone phone = new Phone();
        phone.setUser(expectedUser);
        phone.setCityCode("01");
        phone.setCountryCode("02");
        phone.setNumber("57449726");
        expectedUser.setPhones(new ArrayList<Phone>() {{
            add(phone);
        }});

        expectedUser = om.readValue(mockMvc.perform(post("/api/user/v1/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(expectedUser)))

                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), User.class);
        assertThat(expectedUser.getId()).isNotNull();
    }


    @Test
    @WithMockUser(username = "test", password = "test")
    public void testDisable() throws Exception {
        User expectedUser = new User();
        expectedUser.setName("jerry");
        expectedUser.setEmail("jerry2@gmail.com");
        expectedUser.setPassword("Abcgfhgyui12");
        Phone phone = new Phone();
        phone.setUser(expectedUser);
        phone.setCityCode("01");
        phone.setCountryCode("02");
        phone.setNumber("57449726");
        expectedUser.setPhones(new ArrayList<Phone>() {{
            add(phone);
        }});

        expectedUser = om.readValue(mockMvc.perform(post("/api/user/v1/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(expectedUser)))

                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), User.class);

        expectedUser = om.readValue(mockMvc.perform(post("/api/user/v1/disable/" + expectedUser.getId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString(), User.class);

        assertThat(expectedUser.getIsActive()).isFalse();
    }

}
